use std::process;
use std::process::Command;
use std::process::Stdio;

pub fn launch_dmenu(list: &String, prompt: &str, lines: &u8) -> String {
    let echo_str: String;

    if list.is_empty() {
        echo_str = format!("{}Salir", list);
    } else {
        echo_str = format!("{}\nSalir", list);
    }

    let cmd = Command::new("echo")
        .arg(echo_str)
        .stdout(Stdio::piped())
        .spawn()
        .unwrap();

    let cmd2;

    if lines > &0 {
        cmd2 = Command::new("dmenu")
            .arg("-i")
            .arg("-p")
            .arg(prompt)
            .arg("-l")
            .arg(lines.to_string())
            .stdin(cmd.stdout.unwrap())
            .output()
            .unwrap();
    } else {
        cmd2 = Command::new("dmenu")
            .arg("-i")
            .arg("-p")
            .arg(prompt)
            .stdin(cmd.stdout.unwrap())
            .output()
            .unwrap();
    }

    let mut res = String::from_utf8(cmd2.stdout).unwrap();
    // Removemos el \n de la salida
    res.pop();

    if let "Salir" = &*res {
        process::exit(0);
    }

    res
}

pub fn launch_notify(message: &str, mode: &str) {
    if mode == "low" || mode == "normal" || mode == "critical" {
        if !message.is_empty() {
            Command::new("notify-send")
                .arg("-u")
                .arg(mode)
                .arg(message)
                .output()
                .unwrap();
        }
    }
}
