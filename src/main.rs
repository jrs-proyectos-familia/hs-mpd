use mpd::Client;

use getopts::Matches;
use getopts::Options;

use std::env;
use std::process;

mod menus;
mod command;
mod playlists;
mod songs;
mod tools;

use crate::menus::*;

// __  __ ___ ___
//|  \/  | _ \   \
//| |\/| |  _/ |) |
//|_|  |_|_| |___/
//

pub struct Mpd {
    pub conn: Client,
    options: Options,
    matches: Option<Matches>,
}

impl Mpd {
    fn get_conn() -> Client {
        Client::connect("127.0.0.1:6600").unwrap()
    }

    fn get_options() -> Options {
        let mut options = Options::new();

        options.optflag(
            "a",
            "play songs",
            "Menu para reproducir canciones dependiendo de un criterio",
        );
        options.optflag("h", "help", "Muestra las opciones a usar en el script");

        options
    }

    fn new() -> Mpd {
        Mpd {
            conn: Mpd::get_conn(),
            options: Mpd::get_options(),
            matches: None,
        }
    }

    fn print_usage(&self, program: &str) {
        let usage = format!("{} [-h] -- Script que contiene una lista de tareas para manipular las canciones y audios con MPD.
                        \nDefault\n   {} -h", program, program);

        print!("{}", self.options.usage(&usage));
    }

    fn get_matches(&mut self, args: &[String], program: &str) {
        self.matches = match self.options.parse(&args[0..]) {
            Ok(m) => Some(m),
            Err(e) => {
                if let Some(flag) = e.to_string().split(" ").last() {
                    println!(
                        "Frag {} no disponible.\nPrueba en usar: {} -h",
                        flag, program
                    );
                }
                process::exit(1);
            }
        };
    }
}


// __  __   _   ___ _  _
//|  \/  | /_\ |_ _| \| |
//| |\/| |/ _ \ | || .` |
//|_|  |_/_/ \_\___|_|\_|
//

fn main() {
    let args: Vec<String> = env::args().collect();
    // Nombre del script
    let program = args[0].clone();

    // Creamos el objecto MPD
    let mut mpd = Mpd::new();

    // Actualizamos mpd
    mpd.conn.update().unwrap();

    // Recogemos todas las coincidencias entre mis opciones y los argumentos escritos por consola
    mpd.get_matches(&args[1..], &program);

    match &mpd.matches {
        Some(m) => {
            // [-h]
            if m.opt_present("h") {
                mpd.print_usage(&program);
                process::exit(0);
            }

            if m.opt_present("a") {
                menu_init();
                //playlists(&mut mpd);
            }

            // Si no logró filtrar un flag, entonces mostrar por defecto la ayuda
            mpd.print_usage(&program);
        }
        None => {
            println!("Hubo un error con el manejo de matches");
            process::exit(1);
        }
    }
}
