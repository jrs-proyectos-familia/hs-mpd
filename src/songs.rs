use mpd::search::Query;
use mpd::song::Song;
use mpd::Client;
use std::process;

use crate::Mpd;
use crate::playlists::MyPlaylists;

pub struct MySongs {
    pub s_raw: Vec<Song>,
    pub s_dmenu: String,
    pub s_artist_raw: Vec<Song>,
}

impl MySongs {
    // Método que construye un objeto MySongs
    pub fn new() -> MySongs {
        MySongs {
            s_raw: MySongs::all_songs(&mut Mpd::get_conn()),
            s_dmenu: "".to_string(),
            s_artist_raw: Vec::new(),
        }
    }

    // Método privado que recolecta todas las canciones de tu carpeta de musica (Configurada por MPD)
    fn all_songs(conn: &mut Client) -> Vec<Song> {
        let mut query = Query::new();
        let query = query.and(mpd::Term::Any, "");
        let the_songs = conn.search(query, None);

        match the_songs {
            Ok(songs) => return songs,
            Err(e) => {
                println!(
                    "Hubo un problema al intentar conseguir todas las canciones del usuario \n {}",
                    e
                );
                process::exit(1);
            }
        }
    }
    // Método que recolecta todas las canciones disponibles para añadir a una playlist. La principal diferencia rádica en que en este método va separando todas aquellas canciones existentes en la playlist, evitando así la duplicación de canciones en la playlist particular
    pub fn all_songs_playlist(&mut self, playlist: &MyPlaylists) {
        let mut songs: Vec<Song> = Vec::new();

        if let Some(pl_songs) = &playlist.pl_songs_raw {
            let mut song_exists_playlist = false;

            self.s_raw.iter().for_each(|s| {
                for pl_s in pl_songs {
                    song_exists_playlist = false;

                    if pl_s.file == s.file {
                        song_exists_playlist = true;
                        break;
                    }
                }

                if !song_exists_playlist {
                    songs.push(s.clone());
                }
            });
        }

        self.s_raw = songs;
    }

    // Función que devuelve todas las canciones de un autor particular
    pub fn all_songs_by_artist (&mut self, artist: &String) {
        let mut songs: Vec<Song> = Vec::new();

        self.s_raw.iter().for_each(|s| {
            // Filtramos todas las canciones del artista elegido
            if let Some(s_artist) = s.tags.get("Artist") {
                if s_artist.as_str() == artist {
                    songs.push(s.clone());
                }
            }
        });

        self.s_artist_raw = songs;
    }

    // Función que devuelve todas las canciones de un autor particular No repetidas en la playlist elegida
    pub fn all_songs_playlist_by_artist(&mut self, playlist: &MyPlaylists, artist: &String) {
        let mut songs: Vec<Song> = Vec::new();
        let mut song_exists_playlist = false;

        if let Some(pl_songs) = &playlist.pl_songs_raw {
            self.s_raw.iter().for_each(|s| {
                // Filtramos todas las canciones del artista elegido
                if let Some(s_artist) = s.tags.get("Artist") {
                    if s_artist.as_str() == artist {
                        // Filtramos aquellas canciones unicas no existentes en la playlist de dicho artista
                        song_exists_playlist = false;

                        for pl_s in pl_songs {
                            if pl_s.file == s.file {
                                song_exists_playlist = true;
                                break;
                            }
                        }

                        if !song_exists_playlist {
                            songs.push(s.clone());
                        }
                    }
                }
            })
        }

        self.s_artist_raw = songs;
    }

    // Método que transforma todas las canciones del list_raw a un formato para dmenu
    pub fn ls_songs(&mut self, mode: &str) {
        let mut s_dmenu = String::new();

        let mut s_position = 0;

        let list_songs: &Vec<Song>;
        
        match mode {
            "Artist" => list_songs = &self.s_artist_raw,
            _ => list_songs = &self.s_raw
        }

        if list_songs.len() > 0 {
            for s in list_songs {
                s_position += 1;

                let mut song_description = String::new();

                if let Some(artist) = s.tags.get("Artist") {
                    song_description.push_str(&format!("ﴁ {} ", artist));
                }

                if let Some(title) = &s.title {
                    song_description.push_str(&format!("|  {} ", title));
                }

                song_description.push_str(&format!("|  {}", s.file));

                s_dmenu.push_str(&format!("{}) {} \n", s_position, song_description));
            }
        }

        // Remover ultimo \n
        s_dmenu.pop();

        self.s_dmenu = s_dmenu;
    }

    pub fn get_authors(&mut self) -> Vec<String> {
        let mut authors: Vec<String> = Vec::new();

        self.s_raw.iter().for_each(|s| {
            if let Some(artist) = s.tags.get("Artist") {
                if !authors.contains(&artist) {
                    authors.push(artist.clone());
                }
            }
        });

        return authors;
    }
}
