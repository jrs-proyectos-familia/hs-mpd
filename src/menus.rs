use std::process;

use rand::{self, Rng};

use mpd::status::State;

use crate::command::*;
use crate::playlists::*;
use crate::songs::*;
use crate::tools::*;
use crate::Mpd;

pub fn menu_init() {
    loop {
        // Menú Inicial
        match launch_dmenu(
            &format!(" Ver\n Playlists\nﴁ Artistas\n Random\n Limpiar\n Controles"),
            "Musica Hakyn Seyer",
            &0,
        )
        .as_str()
        {
            " Ver" => {
                if let Ok(current_songs) = Mpd::get_conn().songs(..) {
                    loop {
                        let opt_dmenu = launch_dmenu(
                            &format!("{}", songs_to_dmenu(&current_songs)),
                            &format!(" {} Canciones", current_songs.len()),
                            &5,
                        );

                        if opt_dmenu.is_empty() {
                            break;
                        }
                    }
                }
            }
            " Playlists" => {
                menu_playlist();
            }
            "ﴁ Artistas" => {
                menu_artist();
            }
            " Random" => {
                menu_random();
            }
            " Limpiar" => {
                Mpd::get_conn().clear().unwrap();
                continue;
            }
            " Controles" => {
                menu_controls();
            }
            opt_dmenu => {
                if opt_dmenu.is_empty() {
                    break;
                }
            }
        };
    }

    process::exit(0);
}

fn menu_controls() {
    let mut mpd = Mpd::new();

    loop {
        let status = mpd.conn.status().unwrap();

        let mut random_status = "Activo";
        let mut repeat_status = "Activo";

        if !&status.repeat {
            repeat_status = "Inactivo";
        }

        if !&status.random {
            random_status = "Inactivo";
        }

        match launch_dmenu(
            &format!(
                " Estado\n Random {}\n凌 Repeat {}\n奄 Volumen {}",
                random_status, repeat_status, &status.volume
            ),
            "¿Limpiar reproductor?",
            &0,
        )
        .as_str()
        {
            " Estado" => {
                let status_mode;

                match &status.state {
                    State::Stop => status_mode = "Detenido",
                    State::Play => status_mode = "Tocando",
                    State::Pause => status_mode = "Pausado",
                }

                launch_notify(
                    format!(
                        "Volumen Mpd: \"{}\", Estado \"{}\", Repeat \"{}\", Random \"{}\"",
                        &status.volume, &status_mode, &random_status, &repeat_status
                    )
                    .as_str(),
                    "normal",
                )
            }
            control => {
                if control.is_empty() {
                    break;
                }

                let control_filter: Vec<&str> = control.split(" ").collect();

                if control_filter.len() == 1 {
                    continue;
                }

                match control_filter[1] {
                    "Repeat" => {
                        mpd.conn.repeat(!&status.repeat).unwrap();
                    }
                    "Random" => {
                        mpd.conn.random(!&status.random).unwrap();
                    }
                    "Volumen" => {
                        let vol = launch_dmenu(
                            &"".to_string(),
                            &format!("奄 {} actual | Definir", &status.volume),
                            &0,
                        );

                        if let Ok(num) = vol.parse::<i8>() {
                            mpd.conn.volume(num).unwrap();
                        }
                    }
                    _ => {
                        break;
                    }
                }
            }
        }
    }
}

fn menu_artist() {
    let mut mpd = Mpd::new();

    // Creamos el objeto Songs
    let mut my_songs = MySongs::new();

    // Conseguimos todos los autores de todas las canciones
    let mut authors = my_songs.get_authors();

    // Preguntamos si desea limpiar la lista actual de canciones del reproductor
    loop {
        match launch_dmenu(&format!("No\nSi"), "¿Limpiar reproductor?", &0).as_str() {
            "Si" => {
                mpd.conn.clear().unwrap();
                break;
            }
            "No" => break,
            opt_dmenu => {
                if opt_dmenu.is_empty() {
                    return;
                }
                continue;
            }
        }
    }

    // Control de artistas añadidos (Para eliminar duplicados)
    let mut last_artist_added: String = "".to_string();

    loop {
        // Si ya no hay artistas entonces terminar el proceso de añadir
        if authors.len() == 1 {
            break;
        }

        // Eliminamos todos los artistas ya agregados de nuestra lista dmenu (Para impedir repetidos)
        for index in 0..authors.len() {
            if authors[index] == last_artist_added {
                authors.remove(index);
                break;
            }
        }

        match launch_dmenu(
            &format!(" Ver\n Reproducir\n{}", &vector_to_dmenu(&authors)),
            "ﴁ Artista",
            &0,
        )
        .as_str()
        {
            " Ver" => {
                if let Ok(current_songs) = mpd.conn.songs(..) {
                    loop {
                        let opt_dmenu = launch_dmenu(
                            &format!("{}", songs_to_dmenu(&current_songs)),
                            &format!(" {} Canciones", current_songs.len()),
                            &5,
                        );

                        if opt_dmenu.is_empty() {
                            break;
                        }
                    }
                }
            }
            " Reproducir" => break,
            artist_dmenu => {
                // Si no seleccionó un autor enviar atras
                if artist_dmenu.is_empty() {
                    break;
                }
                // Si la elección que escribió no se encuentra dentro de las opciones, entonces resetear
                if !authors.contains(&artist_dmenu.to_string()) {
                    continue;
                }

                // Guardamos todas las canciones disponibles para agregar a nuestra playlist de un artista particular
                my_songs.all_songs_by_artist(&artist_dmenu.to_string());

                // Agregamos las canciones al reproductor principal
                for song in &my_songs.s_artist_raw {
                    mpd.conn.push(song).unwrap();
                }

                // Guardamos el artista en nuestro vector para impedir repetidos
                last_artist_added = artist_dmenu.clone().to_string();
            }
        }
    }

    // Reproducimos las canciones
    mpd.conn.play().unwrap();
}

fn menu_random() {
    let mut mpd = Mpd::new();

    // Creamos el objeto Songs
    let my_songs = MySongs::new();

    let total_songs: usize = my_songs.s_raw.len();
    let random_songs;

    loop {
        // Preguntamos por el número de canciones para generar aleatoriamente
        let my_number = launch_dmenu(
            &"".to_string(),
            &format!("Disponibles {}  | ¿Cantidad a generar?", total_songs),
            &0,
        );
        // Si no escribió nada, entonces enviar atras
        if my_number.is_empty() {
            return;
        }

        match &my_number {
            num_dmenu => match num_dmenu.parse::<usize>() {
                Ok(num) => {
                    if num > total_songs {
                        launch_notify(
                            format!(
                                "Insuficientes canciones. Solo se disponen de {} canciones",
                                total_songs
                            )
                            .as_str(),
                            "critical",
                        );
                        continue;
                    }
                    random_songs = num;
                    break;
                }
                Err(_) => {
                    launch_notify(
                        format!("Solo se permiten números positivos en el campo").as_str(),
                        "critical",
                    );
                    continue;
                }
            },
        }
    }

    if random_songs == 0 {
        return;
    }

    // Limpiamos el reproductor
    mpd.conn.clear().unwrap();

    let mut ids_unique_songs: Vec<usize> = Vec::new();

    // Id de posición de una canción en particular
    let mut pos_song;

    // Variable general para la creación de números aleatorios
    let mut rng = rand::thread_rng();

    loop {
        if ids_unique_songs.len() == random_songs {
            break;
        }

        // Generación del id random
        pos_song = rng.gen_range(0, total_songs);

        // Validación del id (Impedir que existan repetidos)
        if ids_unique_songs.contains(&pos_song) {
            continue;
        }

        // Validación de la canción con id (Checar si el id corresponde a una canción en particular)
        if let Some(song) = &my_songs.s_raw.get(pos_song) {
            // Si pasó la validación del id entonces guardarlo en su vector
            ids_unique_songs.push(pos_song.clone());

            // Enviamos la canción al reproductor
            mpd.conn.push(song).unwrap();
        } else {
            launch_notify(
                format!("Hubo un error al intentar procesar la canción de forma random").as_str(),
                "critical",
            );
            process::exit(1);
        }
    }

    // Reproducimos las canciones
    mpd.conn.play().unwrap();
}

fn menu_playlist() {
    let mut mpd = Mpd::new();

    loop {
        // Creamos el objeto MyPlaylist
        let mut my_playlists = MyPlaylists::new();

        // Guardamos las playlists
        my_playlists.ls_playlists();

        // Guardamos la playlist elegida
        my_playlists.pl_chosen = launch_dmenu(
            &format!("{}\n Nueva", &my_playlists.pl_dmenu),
            "Mis Playlists ",
            &0,
        );

        match &my_playlists.pl_chosen.as_str() {
            &" Nueva" => {
                // Nombre de la playlist
                let new_playlist = launch_dmenu(&"".to_string(), "Nombre de la playlist : ", &0);

                if !new_playlist.is_empty() {
                    // Limpiamos las canciones actuales (Importante: si no se limpian, entonces las canciones que esten en la lista de reproducción se añadirán a la nueva playlist creada)
                    mpd.conn.clear().unwrap();
                    // Creamos la playlist vacía
                    mpd.conn.save(&new_playlist).unwrap();
                } else {
                    launch_notify(
                        format!("No se agregó un nombre a la playlist ").as_str(),
                        "critical",
                    );
                }
                continue;
            }
            opt_dmenu => {
                // Si no se logró capturar correctamente alguna opción del menu entonces volvemos atrás
                if opt_dmenu.is_empty() {
                    break;
                }

                // Capturamos todas las playlists creadas y disponibles para su uso
                let current_playlists: &Vec<&str> = &my_playlists.pl_dmenu.split("\n").collect();

                // Si el usuario teclea una opción invalida que no cumpla con alguna playlist
                if !current_playlists.contains(&opt_dmenu) {
                    continue;
                }

                // Lanzamos el menu de las playlist activas
                menu_playlist_chosen(&mut my_playlists);
            }
        }
    }
}

fn menu_playlist_chosen(my_playlists: &mut MyPlaylists) {
    loop {
        // Mostramos las diferentes opciones para la playlist elegida
        match launch_dmenu(
            &" Reproducir\n Ver\n Añadir\n Borrar\n Destruir".to_string(),
            &format!(" {}", &my_playlists.pl_chosen).to_string(),
            &0,
        )
        .as_str()
        {
            " Reproducir" => {
                let mut mpd = Mpd::new();

                // Limpiamos las canciones que se encuentren en la reproducción actual
                mpd.conn.clear().unwrap();
                // Cargamos las canciones de la playlist seleccionada al reproductor
                mpd.conn.load(&my_playlists.pl_chosen, ..).unwrap();
                // Inciamos la reproduccion de las canciones
                mpd.conn.play().unwrap();
            }
            " Ver" => {
                // Listamos todas las canciones que contenga la playlist seleccionada
                see_songs_of_a_playlist(my_playlists);
            }
            " Añadir" => {
                // Añadimos una canción a la playlist seleccionada
                add_song_to_playlist(my_playlists);
            }
            " Borrar" => {
                // Borrar canción de una playlist
                remove_song_of_a_playlist(my_playlists);
            }

            " Destruir" => {
                // Destruir una playlist para siempre
                if let Ok(true) = destroy_playlist(my_playlists) {
                    break;
                }
            }
            data => {
                if data.is_empty() {
                    break;
                }

                continue;
            }
        }
    }
}

// Función para visualizar todas las canciones de una determinada playlist
fn see_songs_of_a_playlist(my_playlists: &mut MyPlaylists) {
    loop {
        // Generamos todas las canciones de la playlist seleccionada
        my_playlists.ls_songs_playlist();
        // Obtenemos el numero total de canciones listados en la playlist seleccionada
        let mut number_songs = 0;
        if let Some(songs_vector) = &my_playlists.pl_songs_raw {
            number_songs = songs_vector.len();
        }

        // Mostramos las canciones de la playlist seleccionada
        if launch_dmenu(
            &my_playlists.pl_songs_dmenu,
            &format!(
                " {} |  {} Canciones",
                &my_playlists.pl_chosen, number_songs
            )
            .to_string(),
            &5,
        )
        .is_empty()
        {
            break;
        }
    }
}

// Función que permite añadir una canción a una determinada playlist
fn add_song_to_playlist(my_playlists: &mut MyPlaylists) {
    // Creamos el objeto Songs
    let mut my_songs = MySongs::new();

    loop {
        // Lanzamos el menú de ver lista de canciones por autor o todas
        match launch_dmenu(
            &"ﴁ Artista\n Todas".to_string(),
            &format!(" {} | Buscar por", &my_playlists.pl_chosen).to_string(),
            &0,
        )
        .as_str()
        {
            "ﴁ Artista" => {
                // Conseguimos todos los autores de todas las canciones
                let authors = my_songs.get_authors();

                loop {
                    match launch_dmenu(
                        &vector_to_dmenu(&authors),
                        &format!(" {} | ﴁ Artista", &my_playlists.pl_chosen),
                        &0,
                    )
                    .as_str()
                    {
                        artist_dmenu => {
                            // Si no selecciono un autor enviar atras
                            if artist_dmenu.is_empty() {
                                break;
                            }
                            // Si la elección que escribió no se encuentra dentro de las opciones, entonces resetear
                            if !authors.contains(&artist_dmenu.to_string()) {
                                continue;
                            }

                            // Añadimos la canción a la playlist seleccionada
                            playlists_add_song(
                                my_playlists,
                                &mut my_songs,
                                "Artist",
                                &artist_dmenu.to_string(),
                            )
                        }
                    }
                }
            }
            " Todas" => {
                playlists_add_song(my_playlists, &mut my_songs, "", &"".to_string());
            }
            data => {
                if data.is_empty() {
                    break;
                }
                continue;
            }
        }
    }
}

// Función que añade la canción a la playlist
fn playlists_add_song(
    my_playlists: &mut MyPlaylists,
    my_songs: &mut MySongs,
    mode: &str,
    artist: &String,
) {
    loop {
        // Guardamos las canciones de la playlist seleccionada
        my_playlists.ls_songs_playlist();

        match mode {
            "Artist" => {
                // Guardamos todas las canciones disponibles para agregar a nuestra playlist de un artista particular
                my_songs.all_songs_playlist_by_artist(&my_playlists, &artist);
                // Generamos el menu de opciones de las canciones para dmenu
                my_songs.ls_songs("Artist");
            }
            _ => {
                // Guardamos todas las canciones disponibles para agregar a nuestra playlist
                my_songs.all_songs_playlist(&my_playlists);
                // Generamos el menu de opciones de las canciones para dmenu
                my_songs.ls_songs("");
            }
        }

        let song = launch_dmenu(
            &my_songs.s_dmenu,
            &format!(" {} |  Añadir", &my_playlists.pl_chosen),
            &5,
        );

        let check_res_dmenu: &Vec<&str> = &my_songs.s_dmenu.split("\n").collect();

        if song.is_empty() {
            break;
        } else if !check_res_dmenu.contains(&song.as_str()) {
            continue;
        }

        my_playlists.add_song_to_playlist(&song, &my_playlists.pl_chosen);
    }
}

fn remove_song_of_a_playlist(my_playlists: &mut MyPlaylists) {
    loop {
        // Guardamos las canciones de la playlist seleccionada
        my_playlists.ls_songs_playlist();

        // Mostramos las canciones de la playlist
        let song = launch_dmenu(
            &my_playlists.pl_songs_dmenu,
            &format!(" {} |  Borrar", &my_playlists.pl_chosen).to_string(),
            &5,
        );

        let check_res_dmenu: &Vec<&str> = &my_playlists.pl_songs_dmenu.split("\n").collect();

        if song.is_empty() {
            break;
        } else if !check_res_dmenu.contains(&song.as_str()) {
            continue;
        }

        my_playlists.remove_song_from_playlist(&song, &my_playlists.pl_chosen);
    }
}

fn destroy_playlist(my_playlists: &mut MyPlaylists) -> Result<bool, String> {
    let mut destroy = false;

    loop {
        // mensaje de destruir la playlist
        let yes_no = &"No\nSi".to_string();

        let res = launch_dmenu(
            &yes_no,
            &format!(" {} | ¿Destruir?", &my_playlists.pl_chosen).to_string(),
            &0,
        );

        let check_res_dmenu: &Vec<&str> = &yes_no.split("\n").collect();

        if res.is_empty() || res == "No" {
            break;
        } else if !check_res_dmenu.contains(&res.as_str()) {
            continue;
        } else {
            destroy = true;
            break;
        }
    }

    if destroy {
        Mpd::get_conn().pl_remove(&my_playlists.pl_chosen).unwrap();

        launch_notify(
            format!("Se ha elimando la playlist  {}", &my_playlists.pl_chosen).as_str(),
            "normal",
        );

        return Ok(true);
    }

    Err("No se pudo destruir la playlist".to_string())
}
