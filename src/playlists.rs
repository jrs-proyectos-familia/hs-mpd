use mpd::playlist::Playlist;
use mpd::search::Query;
use mpd::song::Song;
use mpd::Client;
use std::process;

use crate::command::*;
use crate::Mpd;

pub struct MyPlaylists {
    pub pl_raw: Vec<Playlist>,
    pub pl_dmenu: String,
    pub pl_chosen: String,
    pub pl_songs_raw: Option<Vec<Song>>,
    pub pl_songs_dmenu: String,
}

impl MyPlaylists {
    // Método que construye un objeto de tipo MyPlaylists
    pub fn new() -> MyPlaylists {
        MyPlaylists {
            pl_raw: MyPlaylists::all_playlist(&mut Mpd::get_conn()),
            pl_dmenu: "".to_string(),
            pl_chosen: "".to_string(),
            pl_songs_raw: None,
            pl_songs_dmenu: "".to_string(),
        }
    }

    // Método privado que consigue todas las playlists del usuario
    fn all_playlist(conn: &mut Client) -> Vec<Playlist> {
        let the_playlists = conn.playlists();

        match the_playlists {
            Ok(playlists) => return playlists,
            Err(e) => {
                println!(
                    "Hubo un problema al intentar conseguir todas las playlists del usuario \n {}",
                    e
                );
                process::exit(1);
            }
        }
    }

    // Método que transforma todas las playlists del usuario en un formato para dmenu
    pub fn ls_playlists(&mut self) {
        let mut pl_dmenu = String::new();

        for p in &self.pl_raw {
            pl_dmenu.push_str(&format!("{}\n", &p.name));
        }

        // Removemos la última linea
        pl_dmenu.pop();

        self.pl_dmenu = pl_dmenu;
    }

    // Método que consigue todas las canciones de una playlist particular
    pub fn ls_songs_playlist(&mut self) {
        let songs = Mpd::get_conn().playlist(&self.pl_chosen);

        match songs {
            Ok(data) => {
                self.pl_songs_raw = Some(data.clone());

                let mut s_dmenu = String::new();

                let mut s_position = 0;

                for s in &data {
                    s_position += 1;

                    if let Some(title) = &s.title {
                        s_dmenu.push_str(&format!("{})  {} |  {}\n", s_position, title, s.file));
                    }
                }

                s_dmenu.pop();

                self.pl_songs_dmenu = s_dmenu;
            }
            Err(e) => {
                println!(
                    "Hubo un error al intentar conseguir las canciones de la playlist {}\n{}",
                    &self.pl_chosen, e
                );
                process::exit(1);
            }
        }
    }

    // Método que añade una canción a una playlist existente
    pub fn add_song_to_playlist(&self, song: &String, playlist: &String) {
        let mut mpd = Mpd::new();

        // Separamos el titulo de la canción de los demás datos
        if let Some(title_song) = song.split(" ").last() {
            // Validamos que exista un titulo escrito
            if !title_song.is_empty() {
                // Realizamos una busqueda query de la canción en todo el listado de canciones disponibles de MPD
                let mut query = Query::new();
                let query = query.and(
                    //mpd::Term::Tag(std::borrow::Cow::Borrowed("Title")),
                    mpd::Term::File,
                    title_song.trim(),
                );
                let song_db = mpd.conn.search(query, None).unwrap();

                // Validamos que solo encuentre 1 y solo 1 canción original
                if song_db.len() == 1 {
                    if let Ok(_) = mpd.conn.pl_push(playlist, &song_db[0]) {
                        launch_notify(
                            format!(" {} agragada a  {}", title_song.trim(), playlist).as_str(),
                            "normal",
                        );
                    }
                } else {
                    launch_notify(
                        format!("Hay muchas canciones con el nombre  {}", title_song.trim())
                            .as_str(),
                        "critical",
                    );
                }
            } else {
                launch_notify(
                    format!("No hay canción para agregar a  {}", playlist).as_str(),
                    "critical",
                );
            }
        }
    }

    pub fn remove_song_from_playlist(&self, song: &String, playlist: &String) {
        let song_vec: Vec<&str> = song.split(") ").collect();

        if !song_vec[0].is_empty() {
            let song_position = song_vec[0].parse::<u32>().unwrap();
            if let Some(song_title) = song_vec[1].split(" ").last() {
                if let Ok(_) = Mpd::get_conn().pl_delete(playlist, song_position - 1) {
                    launch_notify(
                        format!(" {} removida de  {}", song_title, playlist).as_str(),
                        "normal",
                    );
                } else {
                    launch_notify(
                        format!(
                            "Hubo un error al intentar remover la canción  {} de la playlist  {}",
                            song_title, playlist
                        )
                        .as_str(),
                        "critical",
                    );
                }
            } else {
                launch_notify(
                    format!("Se perdió el titulo de la canción de la playlist  {}", playlist).as_str(),
                    "critical",
                )
            }
        } else {
            launch_notify(
                format!("No ha canción para borrar de la playlist  {}", playlist).as_str(),
                "critical",
            )
        }
    }
}
