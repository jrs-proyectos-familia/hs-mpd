use mpd::song::Song;

// Función que convierte un vector de Strings en un String valido para dmenu
pub fn vector_to_dmenu(vec: &Vec<String>) -> String {
    let mut string = String::new();

    for v in vec {
        string.push_str(&format!("{}\n", v));
    }

    // Eliminamos el último \n
    string.pop();

    string
}

pub fn songs_to_dmenu(vec: &Vec<Song>) -> String {
    let mut string = String::new();
    let mut s_position = 0;

    for s in vec {
        s_position += 1;

        if let Some(title) = &s.title {
            string.push_str(&format!("{})  {} |  {}\n", s_position, title, s.file));
        }
    }

    // Eliminamos el último \n
    string.pop();

    string
}
